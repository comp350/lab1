/**
* The CounterModel class stores the counter value
* and counter logic.
*/
class CounterModel
{
  private CounterView view;   // Reference to the view for the model
  private int counter;
  
  // CounterModel constructors.
  // Initialize variables and
  // create a reference to the view.
  CounterModel()
  {
    view = new CounterView();
    counter = 0;
    view.Display(counter);
  }
  
  CounterModel(int initialVal)
  {
    view = new CounterView();
    counter = initialVal;
    view.Display(counter);
  }
  
  /**
  * Increment
  * Increments the counter
  * and calls Display.
  */
  public void Increment()
  {
    view.Display(++counter);
  }
}
