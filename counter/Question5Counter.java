/**
* Test class
*/
class Question5Counter
{
  // ======================
  //     main method
  // ======================
  public static void main(String[] args)
  {
    // Create a new CounterController which in turn creates the other classes
    // Simply count from 5 to 15
    CounterController c = new CounterController(5,15);
    c.Start();
  }
}