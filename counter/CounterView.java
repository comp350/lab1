/**
* The CounterView class simply displays a
* value passed in.
*/
class CounterView
{
  public CounterView()
  {
  }
  
  /**
  * @param counterValue value that is displayed to the console
  */
  public void Display(int counterValue)
  {
    BinaryDisplay(counterValue);
    System.out.println();
  }
  
  /**
  * BinaryDisplay
  * Recursively computes the binary value from a decimal input.
  * Since we get the remainders in reverse input, the newline is
  * output in the calling routine (the Display  method)
  *
  * @param counterValue the Value to display in binary
  */
  public void BinaryDisplay(int counterValue)
  {
    if (counterValue < 2)
    {
      System.out.print(counterValue);
    }
    else
    {
      BinaryDisplay(counterValue / 2);
      System.out.print(counterValue % 2);
    }
  }
}
