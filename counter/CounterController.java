/**
* The CounterController class waits one second
* before calling increment in the CounterModel
*/
class CounterController
{
  private CounterModel model;   // Reference to the model
  private int startValue, endValue;
  
  // CounterController constructors.
  // Initialize variables and
  // create a reference to the model.
  CounterController()
  {
    startValue = 0;
    endValue = 0;
    model = new CounterModel(startValue);
  }
  
  CounterController(int s, int e)
  {
    startValue = s;
    endValue = e;
    model = new CounterModel(startValue);
  }
  
  /**
  * Start
  * Counts from start to end,
  * calling model's increment.
  */
  public void Start()
  {
    int i = startValue;
    while (i<endValue)
    {
      try
      {
        Thread.sleep(1000); // Wait one second
        model.Increment();
        i++;
      }
      catch (Exception e)
      {
        System.out.println("Exception: " + e.getMessage());
      }
    }
  }
}