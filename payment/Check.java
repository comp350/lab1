/**
* The check class extends Payment and adds some new member variables
*/
class Check extends Payment
{
  private String routingNumber;
  private String name;
  
  // Constructor
  public Check(double amount, String number, String name)
  {
    super(amount);
    this.routingNumber = number;
    this.name = name;
  }
  
  // Accessor and mutator methods
  public void setRoutingNumber(String routingNumber)
  {
    this.routingNumber = routingNumber;
  }
  
  public String getRoutingNumber()
  {
    return routingNumber;
  }
  
  public void setName(String name)
  {
    this.name = name;
  }
  
  public String getName()
  {
    return name;
  }
  
  /*
  * toString
  * @return String Indicates amount is check
  **/
  public String toString()
  {
    return("Check routing number: " + routingNumber + ".  Name on check: " + name 
    + ". Amount: " + amount);
  }
}