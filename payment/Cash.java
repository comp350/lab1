/**
* The cash class simply extends Payment.
*/
class Cash extends Payment
{
  public Cash(double amount)
  {
    super(amount);
  }
  
  /**
  * toString
  * @return String Indicates amount is cash
  */
  public String toString()
  {
    return("Cash amount: " + amount);
  }
}