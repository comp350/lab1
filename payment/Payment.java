/**
* This is an abstract class to represent a type of Payment
*/
abstract class Payment
{
  protected double amount;
  
  // Constructors
  public Payment(double amount)
  {
    this.amount = amount;
  }
  
  /*
  * toString
  * @return String description of the payment.
  **/
  public abstract String toString();
}