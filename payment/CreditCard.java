


/**
* The credit card class extends Payment and adds some new member variables
*/
class CreditCard extends Payment
{
  private String number;
  private String expiration;
  private String name;
  
  // Constructor
  public CreditCard(double amount, String name, String number, String expiration)
  {
    super(amount);
    this.number = number;
    this.name = name;
    this.expiration = expiration;
  }
  
  // Accessor and mutator methods
  public void setNumber(String number)
  {
    this.number = number;
  }
  
  public String getNumber()
  {
    return number;
  }
  
  public void setName(String name)
  {
    this.name = name;
  }
  
  public String getName()
  {
    return name;
  }
  
  public void setExpiration(String exp)
  {
    this.expiration = exp;
  }
  
  public String getExpiration()
  {
    return expiration;
  }
  
  /**
  * toString
  * @return String Indicates amount is check
  */
  public String toString()
  {
    return("Credit Card number: " + number + ".  Name on card: " + name +
    ". Expiration Date: " + expiration + ". Amount: " + amount);
  }
}